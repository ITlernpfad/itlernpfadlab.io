---
layout: page
title: Privacy Policy & Legal Notice (Datenschutz & Impressum)
permalink: /privacy/
---

## Legal Notice (Impressum)

Information according to paragraph 5 TMG (Telemediengesetz – Tele Media Act by German law; Angaben gemäß § 5 TMG):

ITlernpfad

33619 Bielefeld

GERMANY

Contact: [https://gitlab.com/contact_derandere/contact_derandere](https://gitlab.com/contact_derandere/contact_derandere)

The european commission provides a platform for online dissense regulation (ODR): 
[https://ec.europa.eu/consumers/odr](https://ec.europa.eu/consumers/odr)
You find our contact information in the legal notive above.

## Copyright

The content and works published on this website are governed by the copyright laws of Germany. Any duplication, processing, distribution or any form of utilisation beyond the scope of copyright law shall require the prior written consent of the author or authors in question.
Copyright 2017 - 2021 ITlernpfad. All rights reserved.

If not stated otherwise, source code and other software published on ITlernpfadOnline ([https://itlernpfad.gitlab.io](https://itlernpfad.gitlab.io)) is licensed under the MIT license. Images, as specified in the individual figure legends, are usually licensed under the terms of the Creative Commons Attribution 4.0 International license (CC BY 4.0), see the sub-page LICENSE of this website for details. 

## Disclaimer 

### Limitation of liability for internal content 

The content of our website [https://itlernpfad.gitlab.io](https://itlernpfad.gitlab.io) has been compiled with meticulous care and to the best of our knowledge. However, we cannot assume any liability for the up-to-dateness, completeness or accuracy of any of the pages.
Pursuant to section 7, para. 1 of the TMG (Telemediengesetz – Tele Media Act by German law), we as service providers are liable for our own content on these pages in accordance with general laws. However, pursuant to sections 8 to 10 of the TMG, we as service providers are not under obligation to monitor external information provided or stored on our website. Once we have become aware of a specific infringement of the law, we will immediately remove the content in question. Any liability concerning this matter can only be assumed from the point in time at which the infringement becomes known to us.

The views and opinions expressed on this blog are purely my own. Any product claim, statistic, quote or other representation about a product or service should be verified with the manufacturer, provider or party in question.

You must accept that You and You alone are responsible for your safety and safety of others in any endeavor in which you engage. You should verify the accuracy of the information provided on [https://itlernpfad.gitlab.io](https://itlernpfad.gitlab.io) and applicability to your project. You should know your limitations of knowledge and experience. Disconnect every circuit from the power supply before working on it and only connect the power supply to tested devices with an intact earthed metal case or a fully insulated case. If you do any work with "main power” such as alternating current power wiring with nominal voltage of greater than 50 Volts (alternating current) or with direct current-driven circuits with a nominal voltage of 120 Volts or above, you should under all circumstances consult a licensed electrician. [https://itlernpfad.gitlab.io](https://itlernpfad.gitlab.io) assumes that you know, what safety precautions and equipment have to be applied. Most instructions, figures and photographs on [https://itlernpfad.gitlab.io](https://itlernpfad.gitlab.io) do not depict safety precautions or equipment, in order to show the project steps more clearly. These projects are not intended for use by children. Program source code and other software provided is not always optimized and may be untested. Use of these instructions, projects, program code, software and suggestions is at your own risk. [https://itlernpfad.gitlab.io](https://itlernpfad.gitlab.io)  disclaim all responsibility for any resulting damage, injury, or expense. It is your responsibility to make sure that your activities comply with applicable laws. In particular, the published code / SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

### Limitation of liability for external links

Our website contains links to the websites of third parties ("external links"). As the content, privacy practices and/or privacy policyies of these third party websites is not under our control, we cannot assume any liability for such external content, privacy practices and/or privacy policies. In all cases, the provider of information of the linked websites is liable for the content, privacy practices and/or privacy policies and accuracy of the information provided. At the point in time when the links were placed, no infringements of the law were recognisable to us. As soon as an infringement of the law becomes known to us, we will immediately remove the link in question.

Provision of these links does not imply any endorsement, non-endorsement, support or commercial gain by [https://itlernpfad.gitlab.io](https://itlernpfad.gitlab.io).
[https://itlernpfad.gitlab.io](https://itlernpfad.gitlab.io) has no affiliations with third parties. Branded content may be regarded as advertisement without affiliation by some legislations and is therefore marked with "[Werbung ohne Auftrag / unpaid advertisement]" at the beginning of the respective post or paragraph.

## Privacy Policy (Datenschutzhinweis)

A visit to our website can result in the storage on the servers of GitLab B.V. and/or its affiliates ("GitLab") of information about the access (date, time, page accessed). This does not represent any analysis of personal data (e.g., name, address or e-mail address). If personal data are collected, this only occurs – to the extent possible – with the prior consent of the user of the website. Any forwarding of the data to third parties without the express consent of the user shall not take place.

Diese Datenschutzerklärung klärt Sie über die Art, den Umfang und Zweck der Verarbeitung von personenbezogenen Daten (nachfolgend kurz "Daten") innerhalb unseres Onlineangebotes und der mit ihm verbundenen Webseiten, Funktionen und Inhalte sowie externen Onlinepräsenzen, wie z.B. meine Social Media Profile auf (nachfolgend gemeinsam bezeichnet als "Onlineangebot").
Im Hinblick auf die verwendeten Begrifflichkeiten, wie z.B. "personenbezogene Daten" oder deren "Verarbeitung" verweisen wir auf die Definitionen im Art. 4 der Datenschutzgrundverordnung (DSGVO).

Data controller ist ITlernpfad, Germany: [https://gitlab.com/contact_derandere/contact_derandere](https://gitlab.com/contact_derandere/contact_derandere)

Data processor ist GitLab B.V., Netherlands: [https://gitlab.com](https://gitlab.com)

### Arten der verarbeiteten Daten
Der Anbieter der Plattform "GitLab Pages", GitLab B.V. und/oder dessen affiliates, kurz "GitLab", erhebt und verarbeitet Daten, wie sie in der Datenschutzerklärung von GitLab genannt sind. Ich selbst (ITlernpfad) erhebe darüber hinaus keine weiteren Daten

GitLab erhebt folgende Daten:
- Nutzungsdaten (z.B. besuchte Webseiten, Zugriffszeiten)
- Meta-/Kommunikationsdaten (z.B. Server-log Dateien inklusive Geräteinformationen (browser user agent inklusive Browser-Typ, Spracheinstellung, Referrer URL (die zuvor besuchte Seite), exit page (auf dieser Seite angeklickte hyperlinks), IP-Adresse).
- Weitere Daten, inklusive persönlicher Daten, wie sie in der [Datenschutzerklärung von GitLab](https://about.gitlab.com/privacy/) genannt sind, sofern der Nutzer in seinem GitLab-Account (Konto für Nutzung von Dienstleistungen von GitLab) angemeldet ist, während er [https://itlernpfad.gitlab.io](https://itlernpfad.gitlab.io) besucht.

### Kategorien betroffener Personen

Besucher und Nutzer des Onlineangebotes, die nicht in einem GitLab-Account angemeldet sind, sowie
Besucher und Nutzer des Onlineangebotes, die in einem GitLab-Account angemeldet sind
(nachfolgend bezeichne ich die betroffenen Personen zusammenfassend auch als "Nutzer").

### Zweck der Verarbeitung:

- Zurverfügungstellung des Onlineangebotes, seiner Funktionen und Inhalte.
- Verbesserung der Performanz durch GitLab.
- Monitoring der Sicherheit der Internetseiten durch GitLab. 
- Nutzung der Daten durch GitLab B.V. für weitere in der Datenschutzerklärung von GitLab genannte Zwecke.

### Verwendete Begrifflichkeiten

"Personenbezogene Daten" sind alle Informationen, die sich auf eine identifizierte oder identifizierbare natürliche Person (im Folgenden "betroffene Person") beziehen; als identifizierbar wird eine natürliche Person angesehen, die direkt oder indirekt, insbesondere mittels Zuordnung zu einer Kennung wie einem Namen, zu einer Kennnummer, zu Standortdaten, zu einer Online-Kennung (z.B. Cookie) oder zu einem oder mehreren besonderen Merkmalen identifiziert werden kann, die Ausdruck der physischen, physiologischen, genetischen, psychischen, wirtschaftlichen, kulturellen oder sozialen Identität dieser natürlichen Person sind.

"Verarbeitung" ist jeder mit oder ohne Hilfe automatisierter Verfahren ausgeführten Vorgang oder jede solche Vorgangsreihe im Zusammenhang mit personenbezogenen Daten. Der Begriff reicht weit und umfasst praktisch jeden Umgang mit Daten.

Als "Verantwortlicher" wird die natürliche oder juristische Person, Behörde, Einrichtung oder andere Stelle, die allein oder gemeinsam mit anderen über die Zwecke und Mittel der Verarbeitung von personenbezogenen Daten entscheidet, bezeichnet.
Maßgebliche Rechtsgrundlagen
Nach Maßgabe des Art. 13 DSGVO teile ich Ihnen die Rechtsgrundlagen meiner Datenverarbeitungen mit. Sofern die Rechtsgrundlage in der Datenschutzerklärung nicht genannt wird, gilt Folgendes: Die Rechtsgrundlage für die Einholung von Einwilligungen ist Art. 6 Abs. 1 lit. a und Art. 7 DSGVO, die Rechtsgrundlage für die Verarbeitung zur Erfüllung meiner rechtlichen Verpflichtungen ist Art. 6 Abs. 1 lit. c DSGVO, und die Rechtsgrundlage für die Verarbeitung zur Wahrung meiner berechtigten Interessen ist Art. 6 Abs. 1 lit. f DSGVO. Für den Fall, dass lebenswichtige Interessen der betroffenen Person oder einer anderen natürlichen Person eine Verarbeitung personenbezogener Daten erforderlich machen, dient Art. 6 Abs. 1 lit. d DSGVO als Rechtsgrundlage.

### Sicherheitsmaßnahmen

Ich treffe nach Maßgabe des Art. 32 DSGVO unter Berücksichtigung des Stands der Technik, der Implementierungskosten und der Art, des Umfangs, der Umstände und der Zwecke der Verarbeitung sowie der unterschiedlichen Eintrittswahrscheinlichkeit und Schwere des Risikos für die Rechte und Freiheiten natürlicher Personen, geeignete technische und organisatorische Maßnahmen, um ein dem Risiko angemessenes Schutzniveau zu gewährleisten.
Zu den Maßnahmen gehören insbesondere die Sicherung der Vertraulichkeit, Integrität und Verfügbarkeit von Daten durch Kontrolle des physischen Zugangs zu den Daten, als auch des sie betreffenden Zugriffs, der Eingabe, Weitergabe, der Sicherung der Verfügbarkeit und ihrer Trennung. Des Weiteren habe ich Verfahren eingerichtet, die eine Wahrnehmung von Betroffenenrechten, Löschung von Daten und Reaktion auf Gefährdung der Daten ermöglichen. Ferner berücksichtige ich den Schutz personenbezogener Daten bereits bei der Entwicklung, bzw. Auswahl von Hardware, Software sowie Verfahren, entsprechend dem Prinzip des Datenschutzes durch Technikgestaltung und durch datenschutzfreundliche Voreinstellungen (Art. 25 DSGVO).
Zusammenarbeit mit Auftragsverarbeitern und Dritten
Sofern ich im Rahmen meiner Verarbeitung Daten gegenüber anderen Personen und Unternehmen (Auftragsverarbeitern oder Dritten) offenbare, sie an diese übermittle oder ihnen sonst Zugriff auf die Daten gewähre, erfolgt dies nur, wenn Sie eingewilligt haben, eine rechtliche Verpflichtung dies vorsieht oder auf Grundlage meiner berechtigten Interessen (z.B. beim Einsatz von Beauftragten, Webhosts, etc.).
Sofern ich Dritte mit der Verarbeitung von Daten auf Grundlage eines sog. "Auftragsverarbeitungsvertrages" beauftrage, geschieht dies auf Grundlage des Art. 28 DSGVO.

### Übermittlungen in Drittländer

Diese Website ist in den Vereinigten Staaten von Amerika (USA) hosted und die Informationen, die gesammelt werden, werden auf den Servern des Webhosts GitLab in den USA gespeichert und verarbeitet. Die Mitarbeiter, Vertragspartner und affiliated Organisationen von GitLab können in den USA oder in anderen Ländern außerhalb des Heimatlandes des Nutzers lokalisiert sein. Bei Nutzung dieser Website, stimmt der Nutzer dem internationalen Transfer der Nutzerdaten durch GitLab zu. Sofern ich Daten in einem Drittland (d.h. außerhalb der Europäischen Union (EU) oder des Europäischen Wirtschaftsraums (EWR)) verarbeite oder dies im Rahmen der Inanspruchnahme von Diensten Dritter oder Offenlegung, bzw. Übermittlung von Daten an Dritte geschieht, erfolgt dies nur, wenn es auf Grundlage Ihrer Einwilligung, aufgrund einer rechtlichen Verpflichtung oder auf Grundlage meiner berechtigten Interessen geschieht. Vorbehaltlich gesetzlicher oder vertraglicher Erlaubnisse, verarbeite oder lasse ich die Daten in einem Drittland nur beim Vorliegen der besonderen Voraussetzungen der Art. 44 ff. DSGVO verarbeiten. D.h. die Verarbeitung erfolgt z.B. auf Grundlage besonderer Garantien, wie der offiziell anerkannten Feststellung eines der EU entsprechenden Datenschutzniveaus (z.B. für die USA durch das "Privacy Shield") oder Beachtung offiziell anerkannter spezieller vertraglicher Verpflichtungen (so genannte "Standardvertragsklauseln").

### Rechte der betroffenen Personen

Sie haben das Recht, eine Bestätigung darüber zu verlangen, ob betreffende Daten verarbeitet werden und auf Auskunft über diese Daten sowie auf weitere Informationen und Kopie der Daten entsprechend Art. 15 DSGVO.
Sie haben entsprechend. Art. 16 DSGVO das Recht, die Vervollständigung der Sie betreffenden Daten oder die Berichtigung der Sie betreffenden unrichtigen Daten zu verlangen.
Sie haben nach Maßgabe des Art. 17 DSGVO das Recht zu verlangen, dass betreffende Daten unverzüglich gelöscht werden, bzw. alternativ nach Maßgabe des Art. 18 DSGVO eine Einschränkung der Verarbeitung der Daten zu verlangen.
Sie haben das Recht zu verlangen, in die Sie betreffenden Daten, die Sie mir bereitgestellt haben, nach Maßgabe des Art. 20 DSGVO Einsicht zu erhalten und deren Übermittlung an andere Verantwortliche zu fordern.
Sie haben ferner gem. Art. 77 DSGVO das Recht, eine Beschwerde bei der zuständigen Aufsichtsbehörde einzureichen.

### Widerrufsrecht

Sie haben das Recht, erteilte Einwilligungen gem. Art. 7 Abs. 3 DSGVO mit Wirkung für die Zukunft zu widerrufen.

### Widerspruchsrecht

Sie können der künftigen Verarbeitung der Sie betreffenden Daten nach Maßgabe des Art. 21 DSGVO jederzeit widersprechen. Der Widerspruch kann insbesondere gegen die Verarbeitung für Zwecke der Direktwerbung erfolgen.

### Löschung von Daten

Die von mir verarbeiteten Daten werden nach Maßgabe der Art. 17 und 18 DSGVO gelöscht oder in ihrer Verarbeitung eingeschränkt. Sofern nicht im Rahmen dieser Datenschutzerklärung ausdrücklich angegeben, werden die bei mir gespeicherten Daten gelöscht, sobald sie für ihre Zweckbestimmung nicht mehr erforderlich sind und der Löschung keine gesetzlichen Aufbewahrungspflichten entgegenstehen. Sofern die Daten nicht gelöscht werden, weil sie für andere und gesetzlich zulässige Zwecke erforderlich sind, wird deren Verarbeitung eingeschränkt. D.h. die Daten werden gesperrt und nicht für andere Zwecke verarbeitet.

### Nutzung des GitLab-Angebots "GitLab Pages"

Das Onlineangebot wurde erstellt mit der Plattform "GitLab Pages", einem Angebot des Anbieters GitLab B.V., Niederlande, kurz "GitLab". 
Die Plattform "GitLab Pages" dient der Zurverfügungstellung von Infrastruktur- und Plattformdienstleistungen, Rechenkapazität, Speicherplatz und Datenbankdienste, Sicherheitsleistungen sowie technische Wartungsleistungen, die ich zum Zwecke des Betriebs dieses Onlineangebotes einsetze. GitLab verarbeitet dabei Daten über jeden Zugriff auf den Server, auf dem sich dieser Dienst befindet (sogenannte Server log files). Zu den Zugriffsdaten gehören Name (URL) der abgerufenen Webseite, Datum und Uhrzeit des Abrufs, Referrer URL (die zuvor besuchte Seite) und IP-Adresse. GitLab verarbeitet diese Daten in Form von anonymen, aggregierten statistiken, um zum Beispiel zu analysieren, wie stark Github Pages genutzt wird und ob eine bestimmte Seite für viel Datenverkehr sorgt und mehr Resourcen benötigt, sowie zur programmatischen Nutzung bei der Prävention von Missbrauch durch DDoS und ähnliche Attacken auf eine bestimmte Seite. Weitere Informationen zu der Verwendung der Daten durch GitLab erfahren Sie auf der Webseite von GitLab und ihrer [Datenschutzerklärung](https://about.gitlab.com/privacy/).
Dem Internetseitenbetreiber wird keine Einsicht in und Zugriff auf die von GitLab erhobenen Daten gegeben. Die Rechte der betroffenen Personen auf Einsicht, Änderung und Löschung in Bezug auf die über das Onlineangebot erhobenen Daten können damit teilweise gegenüber dem Internetseitenbetreiber und teilweise nur gegenüber dem Anbieter der Plattform, GitLab B.V., geltend gemacht werden. 

### Erhebung von Zugriffsdaten und Logfiles

Der Anbieter der Plattform "GitLab Pages", GitLab B.V., Niederlande, erhebt zur Einhaltung von Verträgen oder auf Grundlage berechtigter Interessen im Sinne des Art. 6 Abs. 1 lit. f. DSGVO Daten über jeden Zugriff auf den Server, auf dem sich dieser Dienst befindet (sogenannte Server log files). Zu den Zugriffsdaten gehören Name (URL) der abgerufenen Webseite, Datei, Datum und Uhrzeit des Abrufs, Referrer URL (die zuvor besuchte Seite) und IP-Adresse. GitLab sammelt diese Daten zur Analyse von anonymen, aggregierten statistiken um zum Beispiel zu analysieren, wie stark Github Pages genutzt wird und ob eine bestimmte Seite für viel Datenverkehr sorgt und mehr Resourcen benötigt, sowie zur programmatischen Nutzung bei der Prävention von Missbrauch durch DDoS und ähnliche Attacken auf eine bestimmte Seite.

### Cookies und Widerspruchsrecht bei Direktwerbung

Als "Cookies" werden kleine Dateien bezeichnet, die auf Rechnern der Nutzer gespeichert werden. Innerhalb der Cookies können unterschiedliche Angaben gespeichert werden. Ein Cookie dient primär dazu, die Angaben zu einem Nutzer (bzw. dem Gerät, auf dem das Cookie gespeichert ist) während oder auch nach seinem Besuch innerhalb eines Onlineangebotes zu speichern.
Als temporäre Cookies bzw. "Session-Cookies" oder "transiente Cookies" werden Cookies bezeichnet, die gelöscht werden, nachdem ein Nutzer ein Onlineangebot verlässt und seinen Browser schließt. In einem solchen Cookie kann z.B. der Inhalt eines Warenkorbs in einem Onlineshop oder ein Login-Status gespeichert werden.
Als "permanent" oder "persistent" werden Cookies bezeichnet, die auch nach dem Schließen des Browsers gespeichert bleiben. So kann z.B. der Login-Status gespeichert werden, wenn die Nutzer diese nach mehreren Tagen aufsuchen. Ebenso können in einem solchen Cookie die Interessen der Nutzer gespeichert werden, die für Reichweitenmessung oder Marketingzwecke verwendet werden. Als "Third-Party-Cookie" werden Cookies bezeichnet, die von anderen Anbietern als dem Verantwortlichen, der das Onlineangebot betreibt, angeboten werden (andernfalls, wenn es nur dessen Cookies sind, spricht man von "First-Party Cookies").

Ich kann temporäre und permanente Cookies einsetzen und kläre hierüber im Rahmen meiner Datenschutzerklärung auf.
Falls die Nutzer nicht möchten, dass Cookies auf ihrem Rechner gespeichert werden, werden sie gebeten, die entsprechende Option in den Systemeinstellungen ihres Browsers zu deaktivieren. Gespeicherte Cookies können in den Systemeinstellungen des Browsers gelöscht werden. Der Ausschluss von Cookies kann zu Funktionseinschränkungen dieses Onlineangebotes führen.

Ein genereller Widerspruch gegen den Einsatz der zu Zwecken des Onlinemarketings eingesetzten Cookies kann bei einer Vielzahl der Dienste, vor allem im Fall des Trackings, über die US-amerikanische Seite [http://www.aboutads.info/choices/](http://www.aboutads.info/choices/) oder die EU-Seite [http://www.youronlinechoices.com/](http://www.youronlinechoices.com/) erklärt werden.

Durch die Verwendung des GitLab-Angebots "GitLab Pages" werden auf diesem Onlineangebot Cookies von GitLab verwendet, um Dienste anzubieten und den Traffic zu analysieren. Es werden die IP-Adresse sowie der User Agent des Nutzers zusammen mit Messwerten zur Leistung und Sicherheit für GitLab freigegeben. Auf diese Weise können Nutzungsstatistiken generiert, Missbrauchsfälle erkannt und behoben und die Qualität des Dienstes gewährleistet werden. Weitere Informationen zu den von GitLab auf diesem Onlineangebot verwendeten Cookies erfahren Sie auf der Webseite von GitLab und ihrer [Datenschutzerklärung](https://about.gitlab.com/privacy/).

### SSL-Verschlüsselung

Dieses Onlineangebot nutzt aus Gründen der Sicherheit und zum Schutz der Übertragung vertraulicher Inhalte eine SSL-Verschlüsselung. Eine verschlüsselte Verbindung ist in der Adresszeile des Browsers am "https://" sowie an einem Schloss-Symbol in der Browserzeile zu erkennen.
Durch die SSL-Verschlüsselung können die Daten, die ein Nutzer an mich übermittelt, nicht von Dritten mitgelesen werden.

### Deaktivierung von Referrern

Über Referrer wird Onlineangeboten mitgeteilt, von welchen Webseiten ihre Nutzer und Besucher kommen. Die Referrer werden dabei nicht nur beim Klick auf einen Link versendet, sondern ebenfalls beim Laden externer Inhalte während des Abrufs eines Onlineangebotes.
Zum Schutz der Daten der Nutzer und Besucher meines Onlineangebotes wurden die Referrer, die von meinem Onlineangebot geschickt werden könnten, deaktiviert.

### Einbindung von Diensten und Inhalten Dritter

Ich setze innerhalb meines Onlineangebotes auf Grundlage meiner berechtigten Interessen (d.h. Interesse an der Analyse, Optimierung und wirtschaftlichem Betrieb meines Onlineangebotes im Sinne des Art. 6 Abs. 1 lit. f. DSGVO) Inhalts- oder Serviceangebote von Drittanbietern ein, um deren Inhalte und Services, wie z.B. Bilder einzubinden (nachfolgend einheitlich bezeichnet als "Inhalte").
Dies setzt immer voraus, dass die Drittanbieter dieser Inhalte die IP-Adresse der Nutzer wahrnehmen, da sie ohne die IP-Adresse die Inhalte nicht an deren Browser senden könnten. Die IP-Adresse ist damit für die Darstellung dieser Inhalte erforderlich. Ich bemühe mich, nur solche Inhalte zu verwenden, deren jeweilige Anbieter die IP-Adresse lediglich zur Auslieferung der Inhalte verwenden.

Besondere Inhalte Dritter, die in diesem Onlineangebot eingebunden sind:

- GitLab Pages (GitLab B.V., Niederlande, [https://www.gitlab.com](https://www.gitlab.com)) - Datenschutzerklärung: [https://about.gitlab.com/privacy/](https://about.gitlab.com/privacy/))

### Kontaktaufnahme

Bei der Kontaktaufnahme mit mir (z.B. per E-Mail oder via sozialer Medien) werden die Angaben des Nutzers zur Bearbeitung der Kontaktanfrage und deren Abwicklung gem. Art. 6 Abs. 1 lit. b) DSGVO verarbeitet. Die Angaben der Nutzer können in einem Customer-Relationship-Management System ("CRM System") oder vergleichbarer Anfragenorganisation gespeichert werden.
Ich lösche die Anfragen, sofern diese nicht mehr erforderlich sind. Ich überprüfe die Erforderlichkeit alle zwei Jahre; ferner gelten die gesetzlichen Archivierungspflichten.

### Onlinepräsenzen in sozialen Medien

Ich unterhalte Onlinepräsenzen innerhalb sozialer Netzwerke und Plattformen, um mit den dort aktiven Besuchern, Interessenten und Nutzern kommunizieren und sie dort über meine Onlineangebote informieren zu können.
Ich weise darauf hin, dass dabei Daten der Nutzer außerhalb des Raumes der Europäischen Union verarbeitet werden können. Hierdurch können sich für die Nutzer Risiken ergeben, weil so z.B. die Durchsetzung der Rechte der Nutzer erschwert werden könnte. Im Hinblick auf US-Anbieter, die unter dem Privacy-Shield zertifiziert sind, weise ich darauf hin, dass sie sich damit verpflichten, die Datenschutzstandards der EU einzuhalten.
Ferner werden die Daten der Nutzer im Regelfall für Marktforschungs- und Werbezwecke verarbeitet. So können z.B. aus dem Nutzungsverhalten und sich daraus ergebenden Interessen der Nutzer Nutzungsprofile erstellt werden. Die Nutzungsprofile können wiederum verwendet werden, um z.B. Werbeanzeigen innerhalb und außerhalb der Plattformen zu schalten, die mutmaßlich den Interessen der Nutzer entsprechen. Zu diesen Zwecken werden im Regelfall Cookies auf den Rechnern der Nutzer gespeichert, in denen das Nutzungsverhalten und die Interessen der Nutzer gespeichert werden. Ferner können in den Nutzungsprofilen auch Daten unabhängig der von den Nutzern verwendeten Geräte gespeichert werden (insbesondere wenn die Nutzer Mitglieder der jeweiligen Plattformen sind und bei diesen eingeloggt sind).
Die Verarbeitung der personenbezogenen Daten der Nutzer erfolgt auf Grundlage meiner berechtigten Interessen an einer effektiven Information der Nutzer und Kommunikation mit den Nutzern gem. Art. 6 Abs. 1 lit. f. DSGVO. Falls die Nutzer von den jeweiligen Anbietern um eine Einwilligung in die Datenverarbeitung gebeten werden (d.h. ihr Einverständnis z.B. über das Anhaken eines Kontrollkästchens oder Bestätigung einer Schaltfläche erklären) ist die Rechtsgrundlage der Verarbeitung Art. 6 Abs. 1 lit. a., Art. 7 DSGVO.
Für eine detaillierte Darstellung der jeweiligen Verarbeitungen und der Widerspruchsmöglichkeiten (Opt-Out), verweise ich auf die nachfolgend verlinkten Angaben der Anbieter.
Auch im Fall von Auskunftsanfragen und der Geltendmachung von Nutzerrechten, weise ich darauf hin, dass diese am effektivsten bei den Anbietern geltend gemacht werden können. Nur die Anbieter haben jeweils Zugriff auf die Daten der Nutzer und können direkt entsprechende Maßnahmen ergreifen und Auskünfte geben. Sollten Sie dennoch Hilfe benötigen, dann können Sie sich an mich wenden.

- GitLab (GitLab B.V., Niederlande, [https://www.gitlab.com](https://www.gitlab.com)) – Datenschutzerklärung: [https://about.gitlab.com/privacy/](https://about.gitlab.com/privacy/).
