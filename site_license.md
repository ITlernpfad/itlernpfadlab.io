---
layout: page
title: License
permalink: /license/
---

Licensing Information User Manual

ITlernpfadOnline ([https://itlernpfad.gitlab.io](https://itlernpfad.gitlab.io))

     __________________________________________________________________

## Introduction

   This License Information User Manual contains the license
   granted by ITlernpfad.

   Last updated: September 2022.

## Licensing Information

   Copyright 2017 - 2022 ITlernpfad

   This is the website by ITlernpfad. This work and it's contents are protected by German copyright law.
   If not stated otherwise, content published on 
   [https://itlernpfad.gitlab.io](https://itlernpfad.gitlab.io), except for images and videos, is
   licensed under the terms of the [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0). To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ OR licensed under the terms of the MIT License. 
   For Images and videos, are not covered by the MIT license. Refer to media metadata
   or the respective figure legend for the legal status of each image and video.
   Most images and videos are licensed under the terms of the 
   [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0).


### MIT License

Copyright 2017 - 2022 ITlernpfad

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
