---
layout: post
title:  "Programming Espressif ESP8266 and ESP32 microcontrollers"
date:   2022-11-07 00:00:00 -0300
author: ITlernpfad
categories: programming
permalink: /programming-ESP32-and-ESP8266/
tags: [programming, C++] 
---

Here I describe how to program the microcontrollers Espressif ESP8266 and ESP32.

## Programming the ESP8266

Development boards that are programmable via USB make life easier. One such board is the D1 mini which comes in different revisions (original, up to version 3) and feature different versions of the ESP8266MOD microcontroller. The [AZ-Delivery D1 Mini ESP8266-Module, also called "D1 mini NodeMcu with ESP8266-12F"](https://www.az-delivery.de/products/d1-mini) is an example.

### Programming the ESP8266 using the Arduino IDE

Az-delivery.de provides instructions how to program the board via USB using the Arduino IDE in their [D1 Mini NodeMcu eBook](https://www.az-delivery.de/products/d1-mini-kostenfreies-e-book).

When using the Arduino IDE, the board definitions have to be installed using the Arduino IDE board manager: 
1. Open the Arduino IDE
2. Select "File" -\> "Preferences"
3. Edit "Additional Boards Manager URLs" and add https://arduino.esp8266.com/stable/package_esp8266com_index.json to the comma-seperated list and click "OK".
4. Select "Tools" -\> "Boards" -\> "Boards Manager" and search for "esp8266" and install in "esp8266" by "ESP8266 Community".
5. Select "Tools" -\> "Board" -\> "Generic ESP8266 module"
6. To program via USB using the Arduino bootloader, select "Tools" -\> "Programmer" -\> "avrISP mkII"
7. Select "Tools" -\> "Port" and select the port the D1 mini board is connected to.
8. Follow [https://arduino-esp8266.readthedocs.io/en/latest/mmu.html](https://arduino-esp8266.readthedocs.io/en/latest/mmu.html)
9. Select "Verify"
10. Select "Upload"

### Programming the ESP8266 using PlatformIO

1. If your operating system is set up to delete the the contents of your `%USERPROFILE%` folder upon restart, edit the system environment variables: Add the system environment variable `PLATFORMIO_CORE_DIR` and/or set it to the desired directory as described at [https://docs.platformio.org/en/latest/envvars.html#directories](https://docs.platformio.org/en/latest/envvars.html#directories). You may set it to any permanent directory with read- and write access.
2. Install Microsoft Visual Studio code and the PlatformIO IDE extension or download and unzip VSC_PlatformIO_Python391.zip from [https://github.com/Jason2866/Portable_VSC_PlatformIO/releases](https://github.com/Jason2866/Portable_VSC_PlatformIO/releases)
3. Install the platform "Espressif 8266" for PlatformIO.
4. Follow [https://docs.platformio.org/en/stable/platforms/espressif8266.html](https://docs.platformio.org/en/stable/platforms/espressif8266.html) and more specifically [https://docs.platformio.org/en/latest/boards/espressif8266/d1_mini.html#board-espressif8266-d1-mini](https://docs.platformio.org/en/latest/boards/espressif8266/d1_mini.html#board-espressif8266-d1-mini)

To explicitely set internal DRAM size and internal cache size, specify build_flags according to [https://docs.platformio.org/en/stable/platforms/espressif8266.html](https://docs.platformio.org/en/stable/platforms/espressif8266.html) (Further details can be found at [https://arduino-esp8266.readthedocs.io/en/latest/mmu.html](https://arduino-esp8266.readthedocs.io/en/latest/mmu.html). A typical platformio.ini file for an Arduino project could look like this:


```
[platformio]
default_env = mini_shared-release

[env:d1_mini]
platform = espressif8266
board = d1_mini
upload_protocol = esptool
framework = arduino

[env:d1_mini_shared-release]
extends = d1_mini
build_type = release
build_flags =
    -D PIO_FRAMEWORK_ARDUINO_MMU_CUSTOM
    -D MMU_IRAM_SIZE=0xC000
    -D MMU_ICACHE_SIZE=0x4000
    -D MMU_IRAM_HEAP
```
Examples can be found here:

- [https://arduino-esp8266.readthedocs.io/en/latest/esp8266wifi/soft-access-point-examples.html](https://arduino-esp8266.readthedocs.io/en/latest/esp8266wifi/soft-access-point-examples.html)
- [https://github.com/esp8266/Arduino/blob/master/libraries/ESP8266WiFi/examples/WiFiAccessPoint/WiFiAccessPoint.ino](https://github.com/esp8266/Arduino/blob/master/libraries/ESP8266WiFi/examples/WiFiAccessPoint/WiFiAccessPoint.ino)
- [https://github.com/platformio/platform-espressif8266/tree/develop/examples/arduino-webserver](https://github.com/platformio/platform-espressif8266/tree/develop/examples/arduino-webserver)

## Programming the ESP32

The Espressif ESP32 DevKits are more capable boards featuring the ESP32 microcontrollers. The [Az-delivery DevKitC V4](https://www.az-delivery.de/products/esp-32-dev-kit-c-v4) is an example.

### Programming the ESP32 using the Arduino IDE

Az-delivery.de provides instructions how to program the board via USB using the Arduino IDE in their [ESP32 Dev Kit C V4 eBook](https://www.az-delivery.de/products/esp-32-dev-kit-c-v4-1).

When using the Arduino IDE, the board definitions have to be installed using the Arduino IDE board manager: 
1. Open the Arduino IDE
2. Select "File" -\> "Preferences"
3. Edit "Additional Boards Manager URLs" and add https://dl.espressif.com/dl/package_esp32_index.json to the comma-seperated list and click "OK".
4. Select "Tools" -\> "Boards" -\> "Boards Manager" and search for "esp32" and install in "esp32" by "Espressif Systems".
5. Select "Tools" -\> "Board" -\> "ESP32 Dev Module"
6. To program via USB using the Arduino bootloader, select "Tools" -\> "Programmer" -\> "avrISP mkII"
7. Select "Tools" -\> "Port" and select the port the D1 mini board is connected to.
8. Select "Verify"
9. Select "Upload"

### Programming the ESP32 using PlatformIO

1. If your operating system is set up to delete the the contents of your `%USERPROFILE%` folder upon restart, edit the system environment variables: Add the system environment variable `PLATFORMIO_CORE_DIR` and/or set it to the desired directory as described at [https://docs.platformio.org/en/latest/envvars.html#directories](https://docs.platformio.org/en/latest/envvars.html#directories). You may set it to any permanent directory with read- and write access.
2. Install Microsoft Visual Studio code and the PlatformIO IDE extension or download and unzip VSC_PlatformIO_Python391.zip from [https://github.com/Jason2866/Portable_VSC_PlatformIO/releases](https://github.com/Jason2866/Portable_VSC_PlatformIO/releases)
3. Install the platform "Espressif 32" for PlatformIO.
4. Follow [https://docs.platformio.org/en/latest/platforms/espressif32.html](https://docs.platformio.org/en/latest/platforms/espressif32.html) and more specifically [https://docs.platformio.org/en/latest/boards/espressif32/az-delivery-devkit-v4.html](https://docs.platformio.org/en/latest/boards/espressif32/az-delivery-devkit-v4.html)

Accordingly, a typical platformio.ini for an Arduino project would be
```
[platformio]
default_envs = devkit-v4-release

[env:az-delivery-devkit-v4]
platform = espressif32
board = az-delivery-devkit-v4
framework = arduino

[env:devkit-v4-release]
extends = az-delivery-devkit-v4
build_type = release
```

A typical platformio.ini for a C99 project using the Espressif 32 platform and the ESP-IDF framework could look like this:
```
[platformio]
default_envs = devkit-v4-release

[env:az-delivery-devkit-v4]
platform = espressif32
board = az-delivery-devkit-v4
framework = espidf

[env:devkit-v4-release]
extends = az-delivery-devkit-v4
build_type = release
```

Example projects can be found here:

- [https://github.com/platformio/platform-espressif32/tree/develop/examples/espidf-blink](https://github.com/platformio/platform-espressif32/tree/develop/examples/espidf-blink)
- [https://docs.platformio.org/en/latest/tutorials/espressif32/espidf_debugging_unit_testing_analysis.html](https://docs.platformio.org/en/latest/tutorials/espressif32/espidf_debugging_unit_testing_analysis.html#tutorial-espressif32-espidf-debugging-unit-testing-analysis) 
- [https://github.com/espressif/esp-idf/tree/master/examples/wifi/getting_started/softAP](https://github.com/espressif/esp-idf/tree/master/examples/wifi/getting_started/softAP)
- [https://github.com/espressif/arduino-esp32/blob/master/libraries/WiFi/examples/WiFiAccessPoint/WiFiAccessPoint.ino](https://github.com/espressif/arduino-esp32/blob/master/libraries/WiFi/examples/WiFiAccessPoint/WiFiAccessPoint.ino)
<br/>

Copyright 2022 ITlernpfad
