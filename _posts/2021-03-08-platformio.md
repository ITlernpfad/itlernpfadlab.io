---
layout: post
title:  Using the PlatformIO IDE 
date:   2021-03-07 00:00:00 -0300
author: ITlernpfad
categories: programming
permalink: /platformio
tags: [programming, software-tips, open-source]
---

In the following video tutorial, I explain how to install and use the PlatformIO IDE.

<video width="480" hight="360" controls preload="auto">
  <source src="/assets/PlatformIO1.mp4" type="video/mp4" >
</video>

Video 1: Embedded software development with the PlatformIO IDE. Copyright 2021 ITlernpfad. This video is dual-licensed under the terms of the [Apache License, version 2.0](https://apache.org/licenses/LICENSE-2.0.txt) or the [Creative Commons Attribution International 4.0 license (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/legalcode). This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by/4.0/](https://creativecommons.org/licenses/by/4.0/). The .mp4 file can be downloaded [here](https://gitlab.com/ITlernpfad/itlernpfad.gitlab.io/-/blob/master/assets/PlatformIO1.mp4).

If your operating system is configured to delete the the contents of your `%USERPROFILE%` folder upon restart, you have two options:

A) Befor installation, edit the system environment variables: Add the system environment variable `PLATFORMIO_CORE_DIR` and/or set it to the desired directory as described at [https://docs.platformio.org/en/latest/envvars.html#directories](https://docs.platformio.org/en/latest/envvars.html#directories). You may set it to any permanent directory with read- and write access.

B) Download and unzip VSC_PlatformIO_Python391.zip from [https://github.com/Jason2866/Portable_VSC_PlatformIO/releases](https://github.com/Jason2866/Portable_VSC_PlatformIO/releases)

Some example projects that make use of the PlatformIO build system can be found at [https://gitlab.com/itlernpfad_public](https://gitlab.com/itlernpfad_public).

Have fun.

<br/>

Copyright 2021 ITlernpfad
