---
layout: post
title:  "Enjoy the blog  by ITlernpfad"
date:   2017-12-31 00:00:00 -0300
author: ITlernpfad
categories: ITlernpfad
---
-- Deutsche Version unten --

Hello world!

Welcome to the blog by ITlernpfad ([https://itlernpfad.gitlab.io](https://itlernpfad.gitlab.io)). I write about embedded systems development. Most content will be written in English, the rest in German. Enjoy!

ITlernpfad

---------------------

Willkommen auf dem blog von ITlernpfad ([https://itlernpfad.gitlab.io](https://itlernpfad.gitlab.io)). Ich schreibe über Elektrotechnik und Elektronik, sowie über die Entwicklung von Firmware für Microcontroller. Vieles wird auf Englisch geschrieben sein, manches auch auf Deutsch.

Viel Spaß wünscht:
ITlernpfad 

<br/>

Copyright 2017 ITlernpfad
