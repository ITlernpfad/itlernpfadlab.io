---
layout: page
permalink: /categories/
title: Categories
---

{% comment %}
  Copyright 2020 ITlernpfad
  Based on:
  https://kylewbanks.com/blog/creating-category-pages-in-jekyll-without-plugins
  Copyright 2016 Kyle Banks
{% endcomment %}
<div id="archives">
{% for category in site.categories %}
  <div class="archive-group">
    {% capture category_name %}{{ category | first }}{% endcapture %}
    <div id="#{{ category_name | slugize }}"></div>
    <p></p>

    <h3 class="category-head">{{ category_name }}</h3>
    <a name="{{ category_name | slugize }}"></a>
    {% for post in site.categories[category_name] %}
    <article class="archive-item">
      <a href="{{ site.baseurl }}{{ post.url }}">{{post.title}}</a>
    </article>
    {% endfor %}
  </div>
{% endfor %}
</div>
